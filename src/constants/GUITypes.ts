const MAIN_LOADING: string = 'main_loading';

const GUI_CHOOSE_CONTROLS: string  = 'gui_choose_controls';
const GUI_USE_KEYBOARD: string = 'gui_use_keyboard';
const GUI_USE_CONTROLLER: string = 'gui_use_controller';
const GUI_CONNECT_CONTROLLER: string = 'gui_connect_controller';
const GUI_START_GAME_MENU: string = 'gui_start_game_menu';


export {
    MAIN_LOADING,
    GUI_CHOOSE_CONTROLS,
    GUI_USE_KEYBOARD,
    GUI_USE_CONTROLLER,
    GUI_CONNECT_CONTROLLER,
    GUI_START_GAME_MENU,
}